from transformers import AutoModelForCausalLM, AutoTokenizer


def Stablelm(text):
        tokenizer = AutoTokenizer.from_pretrained('stabilityai/stablelm-2-zephyr-1_6b')
        model = AutoModelForCausalLM.from_pretrained(
            'stabilityai/stablelm-2-zephyr-1_6b',
            device_map="auto"
        )

        prompt = [{"role": "assistant", "content": "Act as a Summarizer which will summarize the given text by the User in to only few lines.Give Output as a few Summarized lines of given text."},{'role': 'user', 'content': f'Consider this text.\n\ntext:{text}\n\n.Now Summarize the given text and give the output.'}]
        inputs = tokenizer.apply_chat_template(
            prompt,
            add_generation_prompt=True,
            return_tensors='pt'
        )

        tokens = model.generate(
            inputs.to(model.device),
            max_new_tokens=1024,
            temperature=0.5,
            do_sample=True
        )

        print(tokenizer.decode(tokens[0], skip_special_tokens=False))



text=""
Stablelm(text)