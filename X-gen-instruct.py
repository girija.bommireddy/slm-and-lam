import torch
from transformers import AutoTokenizer, AutoModelForCausalLM

tokenizer = AutoTokenizer.from_pretrained("Salesforce/xgen-7b-4k-inst", trust_remote_code=True)
model = AutoModelForCausalLM.from_pretrained("Salesforce/xgen-7b-4k-inst", torch_dtype=torch.bfloat16)

header = (
    "A chat between a curious human and an artificial intelligence assistant. "
    "The assistant gives helpful, detailed, and polite answers to the human's questions.\n\n"
)
article = '''
The open-source community has been a significant beneficiary of a diverse ecosystem. Platforms like Hugging Face and initiatives like EleutherAI have created communities around sharing, improving, and deploying small LLM models. This collaborative environment accelerates the development of small LLMs and ensures that the knowledge and benefits are shared. The landscape of small LLMs is evolving rapidly, driven by a combination of technological advancements, growing demand, and a vibrant community of developers and users. As these models become more sophisticated and widespread, they will play an increasingly central role in shaping the future of AI, making it more accessible, efficient, and impactful. The journey of small LLMs is just beginning, and the potential for positive change and innovation is vast, promising a future where AI is not only powerful but also widely available and beneficial for everyone.

These models are set to transform industries by providing powerful tools that were once the domain of only the most resource-rich organizations. Now, they are within reach of a broader audience, from independent developers and startups to educational institutions and non-profits. The significance of mini-giants lies in their potential to democratize AI. By reducing the resource requirements traditionally associated with sophisticated AI applications, they open up opportunities for innovation and development to a much wider community. 

'''  # insert a document here
prompt = f"### Human: Please summarize the following article.\n\n{article}.\n###"

inputs = tokenizer(header + prompt, return_tensors="pt")
sample = model.generate(**inputs, do_sample=True, max_new_tokens=2048, top_k=100, eos_token_id=50256)
output = tokenizer.decode(sample[0])
print(output.strip().replace("Assistant:", ""))
